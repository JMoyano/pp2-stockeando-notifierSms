package pp2.stockeando.notifier;

import Validator.SmsValidator;
import pp2.stockeando.model.UserContact;

public class MockNotifierSms implements Notifier{

	@Override
	public boolean send(String message, UserContact receiver) {
		System.out.println("Enviando SMS");
		return true;
	}


	@Override
	public String getNameNotifier() {
		return "SMS";
	}

	@Override
	public boolean isReceiverInfoValid(UserContact receiver) {
		return SmsValidator.isPhoneNumberValid(receiver.getContactDetail());
	}
	@Override
	public int compareTo(Notifier o) {
		return this.getNameNotifier().compareTo(o.getNameNotifier());
	}

}
