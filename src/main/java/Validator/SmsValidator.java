package Validator;

import pp2.stockeando.validators.Validator;

import java.util.regex.Pattern;

public class SmsValidator extends Validator {

    private static final Pattern  phoneNumberValidatorPattern = Pattern.compile("^(\\+\\d{1,3}[- ]?)?\\d{10}$");

    public static boolean isPhoneNumberValid(String number){
        return phoneNumberValidatorPattern.matcher(number).matches();
    }

}
